print ("---------------------")
print ("	***Python - Functions = Pass by reference ")
print ("			")
# Function definition is here
def changeme( mylist ):
   "This changes a passed list into this function"
   mylist.append([1,2,3,4]);
   print ("Values inside the function: ", mylist)
   return

# Now you can call changeme function
mylist = [5,6,7];
changeme( mylist );
print ("Values outside the function: ", mylist)
#Outputs:
#Values inside the function:  [10, 20, 30, [1, 2, 3, 4]]
#Values outside the function:  [10, 20, 30, [1, 2, 3, 4]]

# Function definition is here
def addme(a, b):
	"This will add two lists"
	print (" Addition values: ", a + b)
	return a+b

a = 5
b = 10
print (" Called Values: ", addme(a,b))

print ("---------------------")
print ("	***Python - Functions = Default arguments ")
print ("			")
# Function definition is here
def printinfo( name, age = 35 ):
   "This prints a passed info into this function"
   print ("Name: ", name)
   print ("Age ", age)
   return;

# Now you can call printinfo function
printinfo( age=50, name="miki" )
printinfo( name="miki" )
print ("---------------------")
print ("	***Python - Functions = Variable-length arguments ")
print ("			")
# Function definition is here
def printinfo(arg1, *vartuple):
	"Python - Functions = Variable-length arguments"
	print("Output is:")
	print(arg1)
	for x in vartuple:
		print(x)
	return
# Now you can call printinfo function
printinfo( 10 )
printinfo( 70, 60, 50 )
print ("---------------------")
print ("	***Python - Functions = Lambda Functions ")
print ("			")
# Function definition is here
sum = lambda arg1, arg2: arg1 + arg2;

# Now you can call sum as a function
print ("Value of total : ", sum( 10, 20 ))
print ("Value of total : ", sum( 20, 20 ))
print ("---------------------")
print ("	***Python - Functions = Scope Variables	 ")
print ("			")
total = 0; # This is global variable.
# Function definition is here
def sum( arg1, arg2 ):
   # Add both the parameters and return them."
   total = arg1 + arg2; # Here total is local variable.
   print ("Inside the function local total : ", total)
   return total;

# Now you can call sum function
sum( 10, 20 );
print ("Outside the function global total : ", total )

print ("---------------------")
print ("	***Python = Load Modules	 ")
print ("			")
# Import module 
import display # Import module display.py
# Now you can call defined function that module as follows
display.print_func("Hi Python")

print ("---------------------")
print ("	***Python = Namespaces and Scoping	 ")
print ("			")
# Money = 2000
# def AddMoney():
   # # Uncomment the following line to fix the code:
   # # global Money
   # Money = Money + 1

# print (Money)
# AddMoney()
# print (Money)

print ("---------------------")
print ("	***Python = dir(math)	 ")
print ("			")
# Import built-in module math
import math

content = dir(math)
print (content)

print ("---------------------")
print ("	***Python = Packages in Python	 ")
print ("			")
# Now import your Phone Package.
import Phone
#breakpoint()
Phone.Pots()
Phone.Isdn()
Phone.G3()









