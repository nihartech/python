print ("---------------------")
print ("	***Python = File Operations	 ")
print ("			")

# Open a file
fo = open("foo.txt", "wb")
print ("Name of the file: ", fo.name)
print ("Closed or not : ", fo.closed)
print ("Opening mode : ", fo.mode)
#print ("Softspace flag : ", fo.softspace)

# Open a file
fo = open("foo.txt", "wb")
print ("Name of the file: ", fo.name)
# Close opend file
fo.close()

# Open a file
fo2 = open("foo.txt", "w")
str = "Python is a great language.\nYeah its great!!!\n"
fo2.write( str);
fo2.write("aaaaaaaaaaaaa")
# Close opend file
fo2.close()

# Open a file
fo = open("foo.txt", "r+")
str = fo.read(11);
print ("Read 11 Characters : ", str)
str = fo.read();
print ("Read String is : ", str)
# Reposition pointer at the beginning once again
print ("Moving File pointer to beginning - seek(0, 0) ")
position = fo.seek(0, 0);
str = fo.read();
print ("Again read String is : ", str)
# Close opend file
fo.close()

print ("---------------------")
print ("	***Python = Rename a File	 ")
print ("			")
import os
# Rename a file from test1.txt to test2.txt
# os.rename( "foo.txt", "test2.txt" )
# Delete file test2.txt
#os.remove("test2.txt")
print("Creating NEW dir: newdir")
#os.mkdir("newdir")
print("Change path to NEW dir: newdir")
os.chdir("newdir")
print ("Get Current Dir:", os.getcwd())
os.chdir("..")
print("Remove dir: newdir")
#os.rmdir("newdir")

print ("---------------------")
print ("	***Python = listdir()	 ")
print ("			")
import os, sys
# Open a file
path = "F:\\Nihar\\testprograms\\PythonStudy"
dirs = os.listdir( path )

# This would print all the files and directories
for file in dirs:
   print (file)



