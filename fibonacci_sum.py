#!/usr/bin/python


def fibonacci(num):
    if num < 0:
        print("Not a valid number")
    if num <= 1:
        return num
    else:
        return fibonacci(num - 1) + fibonacci(num - 2)

num_in_fibonacci = 9
print("Number {0} in the fibonacci sequence is {1}.".format(num_in_fibonacci, fibonacci(num_in_fibonacci)))

