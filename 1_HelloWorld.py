import sys
print ("Hello, Python!") #Hello, Python!  

counter = 100          # An integer assignment
miles   = 1000.0       # A floating point
name    = "John"       # A string

print (counter) #100
print ( " an Integer") # an Integer
print (miles ) #1000.0
print (" a Floating point") # a Floating point
print (name + " a String") #John a String

#Python Strings
print ("---------------------")
print ("	***Python Strings")
print ("			")
str = 'Hello World!'
print (str)          # Prints complete string
print (str[0])       # Prints first character of the string
print (str[2:5])     # Prints characters starting from 3rd to 5th
print (str[2:])      # Prints string starting from 3rd character
print (str * 2)     # Prints string two times
print (str + "TEST") # Prints concatenated string
	#Outputs:
	#Hello World!
	#H
	#llo
	#llo World!
	#Hello World!Hello World!
	#Hello World!TEST
#Python Lists
print ("---------------------")
print ("	***Python Lists")
print ("			")
list = [ 'abcd', 786 , 2.23, 'john', 70.2 ]
tinylist = [123, 'john']
print (list)          # Prints complete list
print (list[0])       # Prints first element of the list
print (list[1:3])     # Prints elements starting from 2nd till 4th 
print (list[2:])      # Prints elements starting from 3rd element
print (tinylist * 2)  # Prints list two times
print (list + tinylist) # Prints concatenated lists
	#Outputs:
	#['abcd', 786, 2.23, 'john', 70.2]
	#abcd
	#[786, 2.23]
	#[2.23, 'john', 70.2]
	#[123, 'john', 123, 'john']
	#['abcd', 786, 2.23, 'john', 70.2, 123, 'john']
#Python Tuples
print ("---------------------")
print ("	***Python Tuples")
print ("			")
tuple = ( 'abcd', 786 , 2.23, 'john', 70.2  )
tinytuple = (123, 'john')

print (tuple)           # Prints complete list
print (tuple[0])        # Prints first element of the list
print (tuple[1:3])      # Prints elements starting from 2nd till 3rd 
print (tuple[2:])       # Prints elements starting from 3rd element
print (tinytuple * 2)   # Prints list two times
print (tuple + tinytuple) # Prints concatenated lists
	#Outputs:
	#('abcd', 786, 2.23, 'john', 70.2)
	#abcd
	#(786, 2.23)
	#(2.23, 'john', 70.2)
	#(123, 'john', 123, 'john')
	#('abcd', 786, 2.23, 'john', 70.2, 123, 'john')
#Python Dictionary
print ("---------------------")
print ("	***Python Dictionary")
print ("			")
dict = {}
dict['one'] = "This is one"
dict[2]     = "This is two"
tinydict = {'name': 'john','code':6734, 'dept': 'sales'}
print (dict['one'])       # Prints value for 'one' key
print (dict[2])           # Prints value for 2 key
print (tinydict)          # Prints complete dictionary
print (tinydict.keys())   # Prints all the keys
print (tinydict.values()) # Prints all the values
	#Outputs:
	#This is one
	#This is two
	#{'name': 'john', 'code': 6734, 'dept': 'sales'}
	#dict_keys(['name', 'code', 'dept'])
	#dict_values(['john', 6734, 'sales'])
#Python Loops
print ("---------------------")
print ("	***Python Loops")
print ("			")
count = 0
while (count < 9):
   print ('The count is:', count)
   count = count + 1
print ("Good bye!")
	#Outputs:
	#The count is: 0
	#The count is: 1
	#The count is: 2
	#The count is: 3
	#The count is: 4
	#The count is: 5
	#The count is: 6
	#The count is: 7
	#The count is: 8
	#Good bye!
print("	***Using else Statement with Loops")
print ("			")
count = 0
while count < 5:
   print (count, " is  less than 5")
   count = count + 1
else:
   print (count, " is not less than 5")
	#Outputs:
	# 0  is  less than 5
	# 1  is  less than 5
	# 2  is  less than 5
	# 3  is  less than 5
	# 4  is  less than 5
	# 5  is not less than 5
print("	***Single Statement Suites")   
print ("			")
#flag = 1
#while (flag): print ('Given flag is really true!')
#print ("Good bye!")

print("	***nested while loop ")   
print ("			")
i = 2
while(i < 100):
   j = 2
   while(j <= (i/j)):
      if not(i%j): break
      j = j + 1
   if (j > i/j) : print (i, " is prime")
   i = i + 1
print ("Good bye!")
   
print("	***For loop ")   
print ("			")   
for letter in 'Python':     # First Example
   print ('Current Letter :', letter)

fruits = ['banana', 'apple',  'mango']
for fruit in fruits:        # Second Example
   print ('Current fruit :', fruit)
print ("Good bye!")
	#Outputs:
	#Current Letter : P
	#Current Letter : y
	#Current Letter : t
	#Current Letter : h
	#Current Letter : o
	#Current Letter : n
	#Current fruit : banana
	#Current fruit : apple
	#Current fruit : mango
	#Good bye!
print("	***Iterating by Sequence Index")
print ("			")
fruits = ['banana', 'apple',  'mango']
for index in range(len(fruits)):
   print ('Current fruit :', fruits[index])
print ("Good bye!")
	#Outpus:
	#Current fruit : banana
	#Current fruit : apple
	#Current fruit : mango
	#Good bye!
print("	***Using else Statement with Loops")
print ("			")
lower = 10
upper = 20
# uncomment the following lines to take input from the user
#lower = int(input("Enter lower range: "))
#upper = int(input("Enter upper range: "))
print("Prime numbers between",lower,"and",upper,"are:")
for num in range(lower,upper + 1):
   for i in range(2,num):
      if (num % i) == 0:
         break
      else:
         print ('%d is a prime number \n' %(num),)

for letter in 'Python': 
   if letter == 'h':
      pass
      print ('This is pass block')
   print ('Current Letter :', letter)
print ("Good bye!")

import math   # This will import math module
print ("abs(-45) : ", abs(-45)) #abs(-45) :  45
print ("abs(100.12) : ", abs(100.12)) #abs(100.12) :  100.12
#print ("abs(119L) : ", abs(119L))

print ("math.fabs(-45.17) : ", math.fabs(-45.17)) #math.fabs(-45.17) :  45.17
print ("math.fabs(100.12) : ", math.fabs(100.12)) #math.fabs(100.12) :  100.12
print ("math.fabs(100.72) : ", math.fabs(100.72)) #math.fabs(100.72) :  100.72
#print ("math.fabs(119L) : ", math.fabs(119L))
print ("math.fabs(math.pi) : ", math.fabs(math.pi)) #math.fabs(math.pi) :  3.141592653589793


var1 = 'Hello World!'
var2 = "Python Programming"
print ("var1[0]: ", var1[0]) #var1[0]:  H
print ("var1[1]: ", var1[1]) #var1[1]:  e
print ("var1[2]: ", var1[2]) #var1[2]:  l
print ("var1[3]: ", var1[3]) #var1[3]:  l
print ("var1[4]: ", var1[4]) #var1[4]:  o
print ("var1[5]: ", var1[5]) #var1[5]:
print ("var1[6]: ", var1[6]) #var1[6]:  W
print ("var1[1:4]: ", var1[1:4]) #var1[1:4]:  ell
print ("var2[1:5]: ", var2[1:5]) #var2[1:5]:  ytho

print ("---------------------")
print ("	***Python Dictionaries Key-Value pair")
print ("			")
dict1 = {}
dict1 = {'Name': 'Zara', 'Age': 7};
dict2 = {'Name': 'Mahnaz', 'Age': 27};
dict3 = {'Name': 'Abid', 'Age': 27};
dict4 = {'Name': 'Zara', 'Age': 7};
#print ("Return Value : %d" % cmp(dict1, dict2))
#print ("Return Value : %d" % cmp(dict2, dict3))
#print ("Return Value : %d" % cmp(dict1, dict4))
dict = {'Name': 'Zara', 'Age': 7};
print ("Length : %d" % len (dict)) #Length : 2
#print ("Equivalent String : %s" % str (dict))
print ("Variable Type : %s" %  type (dict)) #Variable Type : <class 'dict'>
print ("Start Len : %d" %  len(dict)) #Start Len : 2
dict.clear()
print ("End Len : %d" %  len(dict)) #End Len : 0
dict1 = {'Name': 'Zara', 'Age': 7};
dict2 = dict1.copy()
#print ("New Dictionary : %s" %  str(dict2))
seq = ('name', 'age', 'sex')
dict = dict.fromkeys(seq)
#print( "New Dictionary : %s" %  str(dict))
dict = dict.fromkeys(seq, 10)
#print ("New Dictionary : %s" %  str(dict))
dict = {'Name': 'Zabra', 'Age': 7}
print ("Value dict.get : %s" %  dict.get('Age')) #Value dict.get : 7
print ("Value dict.get Default : %s" %  dict.get('Education', "Never")) #Value dict.get Default : Never
#print ("Value : %s" %  dict.has_key('Age'))
#print ("Value : %s" %  dict.has_key('Sex'))
print ("Value dict.items : %s" %  dict.items()) #Value dict.items : dict_items([('Name', 'Zabra'), ('Age', 7)])
print ("Value dict.keys : %s" %  dict.keys()) #Value dict.keys : dict_keys(['Name', 'Age'])
print ("Value dict.setdefault: %s" %  dict.setdefault('Age', None)) #Value dict.setdefault: 7
print ("Value dict.setdefault: %s" %  dict.setdefault('Sex', None)) #Value dict.setdefault: None

dict2 = {'Sex': 'female' }
dict.update(dict2)
print ("Value : %s" %  dict)	#Value : {'Name': 'Zabra', 'Age': 7, 'Sex': 'female'}
dict = {'Name': 'Zara', 'Age': 7}
print ("Value : %s" %  dict.values()) #Value : dict_values(['Zara', 7])

import time;
localtime = time.localtime(time.time())
print ("Local current time :", localtime) #Local current time : time.struct_time(tm_year=2018, tm_mon=8, tm_mday=31, tm_hour=16, tm_min=26, tm_sec=20, tm_wday=4, tm_yday=243, tm_isdst=1)
localtime = time.asctime( time.localtime(time.time()) )
print ("Local current time :", localtime) #Local current time : Fri Aug 31 16:27:28 2018

import calendar
cal = calendar.month(2018, 10)
print ("Here is the calendar:")
print (cal)
	#outputs:
	#Here is the calendar:
	#    October 2018
	#Mo Tu We Th Fr Sa Su
	# 1  2  3  4  5  6  7
	# 8  9 10 11 12 13 14
	#15 16 17 18 19 20 21
	#22 23 24 25 26 27 28
	#29 30 31
print ("---------------------")
print ("	***Python - Functions = Pass by reference ")
print ("			")
# Function definition is here
def changeme( mylist ):
   "This changes a passed list into this function"
   mylist.append([1,2,3,4]);
   print ("Values inside the function: ", mylist)
   return

# Now you can call changeme function
mylist = [10,20,30];
changeme( mylist );
print ("Values outside the function: ", mylist)
#Outputs:
#Values inside the function:  [10, 20, 30, [1, 2, 3, 4]]
#Values outside the function:  [10, 20, 30, [1, 2, 3, 4]]






















