
print ("---------------------")
print ("	***Python - Socket() - Server side ")
print ("			")

import socket               # Import socket module

s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.
s.bind((host, port))        # Bind to the port

s.listen(5)                 # Now wait for client connection.
conn, addr = s.accept()     # Establish connection with client.
while conn:
	print ('Got connection from', addr)
	while True:
		data = conn.recv(1024)
		if data == 'q':
			conn.close()
		conn.sendall(data)
		