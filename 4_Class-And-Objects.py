print ("---------------------")
print ("	***Python - Classes and Objects ")
print ("			")
class Employee:
   'Common base class for all employees'
   empCount = 0

   def __init__(self, name, salary):
      self.name = name
      self.salary = salary
      Employee.empCount += 1
   
   def displayCount(self):
     print ("Total Employee %d" % Employee.empCount)

   def displayEmployee(self):
      print ("Name : ", self.name,  ", Salary: ", self.salary)
	  
print ("This would create first object of Employee class")
emp1 = Employee("Zara", 2000)
emp1.displayEmployee()
print("This would create second object of Employee class")
emp2 = Employee("Manni", 5000)
emp2.displayEmployee()
print ("Total Employee %d" % Employee.empCount)

emp3 = Employee("Test", 5000)
emp3.displayEmployee()

print ("Employee.__doc__:", Employee.__doc__)
print ("Employee.__name__:", Employee.__name__)
print ("Employee.__module__:", Employee.__module__)
print ("Employee.__bases__:", Employee.__bases__)
print ("Employee.__dict__:", Employee.__dict__)

print ("---------------------")
print ("	***Python - Constructor & Destructor ")
print ("			")
class Point:
   def __init__( self, x=0, y=0):
      self.x = x
      self.y = y
   def __del__(self):
      class_name = self.__class__.__name__
      print (class_name, "...destroyed")
pt1 = Point()
pt2 = pt1
pt3 = pt1
print (id(pt1), id(pt2), id(pt3)) # prints the ids of the obejcts
del pt1
del pt2
del pt3

print ("---------------------")
print ("	***Python - Inheritance ")
print ("			")
class Parent:        # define parent class
   parentAttr = 100
   def __init__(self):
      print ("Calling parent constructor")

   def parentMethod(self):
      print ('Calling parent method')

   def setAttr(self, attr):
      Parent.parentAttr = attr

   def getAttr(self):
      print ("Parent attribute :", Parent.parentAttr)

class Child(Parent): # define child class
   def __init__(self):
      print ("Calling child constructor")

   def childMethod(self):
      print ('Calling child method')

c = Child()          # instance of child
c.childMethod()      # child calls its method
c.parentMethod()     # calls parent's method
c.getAttr() 
c.setAttr(200)       # again call parent's method
c.getAttr()          # again call parent's method

print ("---------------------")
print ("	***Python - Function Overriding ")
print ("			")
class Parent:        # define parent class
   def myMethod(self):
      print( 'Calling parent method')

class Child(Parent): # define child class
   def myMethod(self):
      print ('Calling child method')

c = Child()          # instance of child
c.myMethod()         # child calls overridden method
d = Parent() 
d.myMethod()

print ("---------------------")
print ("	***Python - Function Overloading ")
print ("			")
class Vector:
   def __init__(self, a, b):
      self.a = a
      self.b = b

   def __str__(self):
      return 'Vector (%d, %d)' % (self.a, self.b)
   
   def __add__(self,other):
      return Vector(self.a + other.a, self.b + other.b)

v1 = Vector(2,10)
print ("V1:",v1)
v2 = Vector(5,-2)
print ("V2:",v2)
print (v1 + v2)


print ("---------------------")
print ("	***Python - Search and Replace ")
print ("			")
import re

phone = "adasdas 2004-959-559 # This is Phone Number"

# Delete Python-style comments
num = re.sub(r'#.*$', "", phone)
print ("Phone Num : ", num)

# Remove anything other than digits
num = re.sub(r'\D', "", phone)    
print ("Phone Num : ", num)
