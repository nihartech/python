#!/bin/python

import subprocess
from subprocess import check_output
from subprocess import Popen, STDOUT, PIPE

import os
import os.path
import time
import sys
import errno
import datetime



def run_any_cmd(cmd, *arguments):
    cmdline = [cmd]
    cmdline.extend(arguments)
    print(''.join(arg for arg in cmdline))

    proc = Popen(cmdline, stdout=PIPE, stderr=STDOUT)
    output = []
    while True:
        line = proc.stdout.readline()
        if not line:
            break

        #line = line.rstrip().decode('utf-8') #UnicodeEncodeError: 'ascii' codec can't encode character u'\u2018' in position 64: ordinal not in range(128)
        print('>> {0}'.format(line))
        output.append(line)

    return_code = proc.wait()
    if return_code != 0:
        print()
        print('Command terminated with exit code {0}.'.format(return_code))
        #sys.exit(1) #Commented due to debug

    return output


#ret = run_any_cmd('/usr/bin/ls','-lrt')
ret = run_any_cmd('/usr/bin/grep','-ri','commented')
ret = ''.join([str(r) for r in ret])

print('Output: ',ret)




