
print ("---------------------")
print ("	***Python - Socket() - Client side ")
print ("			")

import socket               # Import socket module

s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.

s.connect((host, port))
s.sendall(b'Hello World')
s.sendall(b'q')
data = s.recv(1024)
print ('Received: ', repr(data))               